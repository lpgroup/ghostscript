# ghostscript

Flatten and optimize PDF with shell command Ghostscript ´gsx´.

## Install

The npm module requirs Ghostscript to be installed on the machine.

```sh
# OSX
brew install ghostscript

# Alpine
apk update
apk add imagemagick ghostscript-fonts ghostscript

# Debian/ubuntu
apt-get update
apt-get install ghostscript
```

Installation of the npm

```sh
echo @lpgroup:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
npm install @lpgroup/ghostscript
```

## Example

```javascript
const fs = require("fs").promises;
const path = require("path");
const { optimize } = require("@lpgroup/ghostscript");

const fileIn = path.resolve("./test/flytt_uppdrag.pdf");
const fileOut = path.resolve("./test/flytt_uppdrag_optimized.pdf");

const input = await fs.readFile(fileIn);
const output = await optimize({ input });
await fs.writeFile(fileOut, output, "binary");
```

## Contribute

OSX: Install correct node version

```sh
# Uninstall node and install nvm
brew uninstall --ignore-dependencies node
brew uninstall --force node
brew update
brew install nvm
mkdir ~/.nvm

# Add to ~/.zshrc or ~/.bash_profile
# export NVM_DIR=~/.nvm
# source $(brew --prefix nvm)/nvm.sh

# Install correct node version
nvm use
nvm install
```

Install packages and test code

```sh
npm install
npm run prettier
npm run test
```

## Raw Command

Below is the raw command if you'd like to optimize it further yourself. Credit goes to
Matt DesLauriers and the repo http://github.com/mattdesl/gsx-pdf-optimize

```sh
cat test/flytt_uppdrag.pdf | \
gsx -sDEVICE=pdfwrite \
-dPDFSETTINGS=/screen \
-dNOPAUSE \
-dQUIET \
-dBATCH \
-dCompatibilityLevel=1.5 \
-dSubsetFonts=true \
-dCompressFonts=true \
-dEmbedAllFonts=true \
-sProcessColorModel=DeviceRGB \
-sColorConversionStrategy=RGB \
-sColorConversionStrategyForImages=RGB \
-dConvertCMYKImagesToRGB=true \
-dDetectDuplicateImages=true \
-dColorImageDownsampleType=/Bicubic \
-dColorImageResolution=300 \
-dGrayImageDownsampleType=/Bicubic \
-dGrayImageResolution=300 \
-dMonoImageDownsampleType=/Bicubic \
-dMonoImageResolution=300 \
-dDownsampleColorImages=true \
-dDoThumbnails=false \
-dCreateJobTicket=false \
-dPreserveEPSInfo=false \
-dPreserveOPIComments=false \
-dPreserveOverprintSettings=false \
-dUCRandBGInfo=/Remove \
-sOutputFile=- - > flytt_uppdrag_optimized.pdf

```

## License

MIT, see [LICENSE.md](https://gitlab.com/lpgroup/ghostscript/blob/master/LICENSE.md) for details.
